//
//  HomeViewController.h
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 16/03/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *fullGameButton;
@property (strong, nonatomic) IBOutlet UIButton *capitalGameButton;
@property (strong, nonatomic) IBOutlet UIButton *flagGameButton;
@property (strong, nonatomic) IBOutlet UIButton *currencyGameButton;
@property (strong, nonatomic) IBOutlet UIButton *continentGameButton;
@property (strong, nonatomic) IBOutlet UIButton *mapGameButton;
- (IBAction)fullGameClick:(id)sender;
- (IBAction)capitalGameClick:(id)sender;
- (IBAction)flagGameClick:(id)sender;
- (IBAction)currencyGameClick:(id)sender;
- (IBAction)continentGameClick:(id)sender;
- (IBAction)mapGameClick:(id)sender;
- (void)showGameOver;
@end
