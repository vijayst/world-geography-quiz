//
//  Country.m
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 20/01/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import "Country.h"
#import "DbHelper.h"

@implementation Country
@synthesize difficulty;
@synthesize name;
@synthesize capital;
@synthesize flag;
@synthesize currency;
@synthesize continent;

+(NSArray *)getCountries
{
    DbHelper *helper = [[DbHelper alloc] init];
    [helper open];
    sqlite3_stmt *statement;
    [helper executeQuery:@"SELECT country, capital, fips, currency, continent, difficulty from country" statement:&statement];
    NSMutableArray *array = [[NSMutableArray alloc] init];
    // int i = 0;
    while (sqlite3_step(statement) == SQLITE_ROW) {
        // if(i++==4) break;
        Country *country = [[Country alloc] init];
        country.name = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
        country.capital = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
        country.flag = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
        country.currency = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
        country.continent = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 4)];
        country.difficulty = sqlite3_column_int(statement,5);
        country.map = [country.flag copy];
        [array addObject:country];
    }
    sqlite3_finalize(statement);
    [helper close];
    return array;
}


@end
