//
//  ViewController.m
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 20/01/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import "GameViewController.h"
#import "HomeViewController.h"
#import "AudioHelper.h"
#import "Points.h"
#import "Translation.h"



@interface GameViewController ()
{
    Translation *translation;
    float iPhoneHeaderFont;
    float iPadHeaderFont;
}
@property (strong, nonatomic) NSString *timeText;
@property (strong, nonatomic) NSString *scoreText;
@property (strong, nonatomic) NSString *levelText;
@property (strong, nonatomic) NSString *maxText;

@property (strong, nonatomic) UIImage *capitalImage;
@property (strong, nonatomic) UIImage *flagImage;
@property (strong, nonatomic) UIImage *currencyImage;
@property (strong, nonatomic) UIImage *continentImage;
@property (strong, nonatomic) UIImage *mapImage;

@property (strong, nonatomic) AudioHelper *audioHelper;
@property (strong, nonatomic) TTSHelper *ttsHelper;
@property (strong, nonatomic) AnimationHelper *animHelper;
@end

@implementation GameViewController
{
    UIColor *_panelColor;
    UIColor *_timeoutColor;
    NSArray *_buttons;
    BOOL _drawImageInChoice;
    BOOL _drawImageInQuestion;
}

static float kImageMargin = 10.0F;

#pragma mark ViewController lifecycle methods
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    [self initLabels];
    matchColor = [UIColor colorWithRed:0 green:1 blue:0 alpha:0.5];
    noMatchColor = [UIColor colorWithRed:1 green:0 blue:0 alpha:0.8];

    
    CGRect rect = [[UIScreen mainScreen] bounds];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]];
    if(rect.size.height==480) {
        CGRect questionFrame = self.questionLabel.frame;
        [self.questionLabel setFrame:CGRectMake(questionFrame.origin.x, questionFrame.origin.y, questionFrame.size.width, questionFrame.size.height-25)];
        CGRect questionInnerFrame = self.questionInnerLabel.frame;
        [self.questionInnerLabel setFrame:CGRectMake(questionInnerFrame.origin.x, questionInnerFrame.origin.y, questionInnerFrame.size.width, questionInnerFrame.size.height-25)];
        CGRect imageFrame = self.imageView.frame;
        [self.imageView setFrame:CGRectMake(imageFrame.origin.x, imageFrame.origin.y-6, imageFrame.size.width-12, imageFrame.size.height-12)];
        CGRect choice1Frame = self.choice1Button.frame;
        [self.choice1Button setFrame:CGRectMake(choice1Frame.origin.x, choice1Frame.origin.y-25, choice1Frame.size.width, choice1Frame.size.height-25)];
        CGRect choice2Frame = self.choice2Button.frame;
        [self.choice2Button setFrame:CGRectMake(choice2Frame.origin.x, choice2Frame.origin.y-25, choice2Frame.size.width, choice2Frame.size.height-25)];
        CGRect choice3Frame = self.choice3Button.frame;
        [self.choice3Button setFrame:CGRectMake(choice3Frame.origin.x, choice3Frame.origin.y-50, choice3Frame.size.width, choice3Frame.size.height-25)];
        CGRect choice4Frame = self.choice4Button.frame;
        [self.choice4Button setFrame:CGRectMake(choice4Frame.origin.x, choice4Frame.origin.y-50, choice4Frame.size.width, choice4Frame.size.height-25)];
        
        CGRect levelFrame = self.levelLabel.frame;
        [self.levelLabel setFrame:CGRectMake(levelFrame.origin.x, levelFrame.origin.y-88, levelFrame.size.width, levelFrame.size.height)];
        CGRect timeFrame = self.timeLabel.frame;
        [self.timeLabel setFrame:CGRectMake(timeFrame.origin.x, timeFrame.origin.y-88, timeFrame.size.width, timeFrame.size.height)];
        CGRect homeFrame = self.homeButton.frame;
        [self.homeButton setFrame:CGRectMake(homeFrame.origin.x, homeFrame.origin.y-88, homeFrame.size.width, homeFrame.size.height)];
        CGRect highFrame = self.highLabel.frame;
        [self.highLabel setFrame:CGRectMake(highFrame.origin.x, highFrame.origin.y-88, highFrame.size.width, highFrame.size.height)];
        CGRect maxFrame = self.maxLabel.frame;
        [self.maxLabel setFrame:CGRectMake(maxFrame.origin.x, maxFrame.origin.y-88, maxFrame.size.width, maxFrame.size.height)];
        [self.view setNeedsDisplay];
    }

    self.capitalImage = [self resize:[UIImage imageNamed:@"capitals.png"]];
    self.flagImage = [self resize:[UIImage imageNamed:@"flags.png"]];
    self.currencyImage = [self resize:[UIImage imageNamed:@"currencies.png"]];
    self.continentImage = [self resize:[UIImage imageNamed:@"continents.png"]];
    self.mapImage = [self resize:[UIImage imageNamed:@"maps.png"]];
    
    iPhone = [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone;
    if(self.audioHelper==nil)
        self.audioHelper = [[AudioHelper alloc] init];
    self.ttsHelper = [[TTSHelper alloc] init];
    self.ttsHelper.delegate = self;
    self.animHelper = [[AnimationHelper alloc] init];
    self.animHelper.delegate = self;
    
    _buttons = @[self.choice1Button, self.choice2Button, self.choice3Button, self.choice4Button];
    for(UIButton *button in _buttons)
    {
        button.titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    self.view.hidden = YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(!self.game.paused)
        [self getDataForNewLevel];
    else {
        // refresh the view based on the game state.
        [self refreshView];
        [self updateButtons];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    // Start the timer only when started from a paused state.
    if(self.game.paused)
        [self createTimer];
    else
    {
        self.view.hidden = NO;
        self.view.userInteractionEnabled = NO;
        self.homeButton.enabled = NO;
        [self animate];
    }
}

#pragma mark UI initialization methods
-(BOOL)getDataForNewLevel
{
    BOOL result = [self.game getQuiz];
    if(!result) {
        id controller = (HomeViewController *)self.presentingViewController;
        [self dismissViewControllerAnimated:NO completion:^{
            [controller showGameOver];
        }];
        return NO;
    }
    
    [self refreshView];
    return YES;
}

-(void)refreshView
{
    _drawImageInQuestion = ((self.game.quiz.type==Flags || self.game.quiz.type==Maps) && self.game.quiz.reverse);
    if(_drawImageInQuestion)
    {
        [self updateLabel:self.questionInnerLabel
                 withImageName:self.game.quiz.question
                  forQuizType:self.game.quiz.type];
    }
    else
    {
        self.questionInnerLabel.text =  self.game.quiz.question;
        self.questionInnerLabel.backgroundColor = [UIColor clearColor];
    }
    
    [self setImage:self.game.quiz.type];
    for(UIButton *button in _buttons)
    {
        [button setTitle:@"" forState:UIControlStateNormal];
    }
    
    UIColor *labelColor;
    switch(self.game.quiz.type)
    {
        case Capitals:
            labelColor = [UIColor colorWithPatternImage:self.capitalImage];
            break;
        case Flags:
            labelColor = [UIColor colorWithPatternImage:self.flagImage];
            break;
        case Currencies:
            labelColor = [UIColor colorWithPatternImage:self.currencyImage];
            break;
        case Continents:
            labelColor = [UIColor colorWithPatternImage:self.continentImage];
            break;
        case Maps:
            labelColor = [UIColor colorWithPatternImage:self.mapImage];
            break;
        case QuizTypeNone:
            break;
    }
    
    _drawImageInChoice = ((self.game.quiz.type==Flags || self.game.quiz.type==Maps) && !self.game.quiz.reverse);
    
    self.choice1Button.backgroundColor = labelColor;
    self.choice2Button.backgroundColor = labelColor;
    self.choice3Button.backgroundColor = labelColor;
    self.choice4Button.backgroundColor = labelColor;
    
    labelColor = self.game.quiz.time > 0 ? _panelColor : _timeoutColor;
    self.levelLabel.backgroundColor = labelColor;
    self.timeLabel.backgroundColor = labelColor;
    self.highLabel.backgroundColor = labelColor;
    self.maxLabel.backgroundColor = labelColor;
    self.homeButton.backgroundColor = labelColor;
    
    headerFontSize = iPhone ? iPhoneHeaderFont : iPadHeaderFont;
    self.levelLabel.attributedText = [self getLevelLabel];
    self.timeLabel.attributedText = [self getTimerLabel];
    self.highLabel.attributedText = [self getScoreLabel];
    self.maxLabel.attributedText = [self getMaxLabel];
}

-(void)animate
{
    [self.audioHelper playGameLoadSound];
    animComplete = false;
    self.choice1Button.enabled = self.choice2Button.enabled
                                = self.choice3Button.enabled
                                = self.choice4Button.enabled
                                = true;
    [self.animHelper zoom:self.questionLabel];
    [self.animHelper zoom:self.questionInnerLabel];
    [self.animHelper zoom:self.imageView];
    [self.animHelper fade:self.imageView];
    [self.animHelper fade:self.choice1Button];
    [self.animHelper fade:self.choice2Button];
    [self.animHelper fade:self.choice3Button];
    [self.animHelper fade:self.choice4Button];
}

-(void)newLevel
{
    BOOL result = [self getDataForNewLevel];
    if(result) {
        [self animate];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        //TODO: When is this called? Remove!?
        self.game = [[Game alloc] init];
        [self newLevel];
    }
}

#pragma mark AnimationCompletedDelegate protocol
-(void)animationCompleted
{
    if(!animComplete) {
        animComplete = true;
        [self.ttsHelper speakText:self.game.quiz.prompt];
    }
}

#pragma mark SpeechCompletedDelegate protocol
-(void)speechCompleted
{
    [self updateButtons];
    self.view.userInteractionEnabled = true;
    self.homeButton.enabled = true;
    // start timer
    startNewLevel = false;
    startNewLevelTime=0;
    [self performSelectorOnMainThread:@selector(createTimer)
                           withObject:nil
                        waitUntilDone:YES];
}

#pragma mark Timer methods
-(void)createTimer
{
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self
                                           selector:@selector(updateTime)
                                           userInfo:Nil repeats:true];
}

-(void)stopTimer
{
    [timer invalidate];
    timer = nil;
}

-(void)pauseGame
{
    [self stopTimer];
    self.game.paused = YES;
}

-(void)resumeGame
{
    if(self.game.paused)
    {
        [self createTimer];
        self.game.paused = NO;
    }
}

-(void)updateTime
{
    if(startNewLevel) {
        if(++startNewLevelTime==3) {
            [self stopTimer];
            [self newLevel];
        }
    } else {
        if(self.game.quiz.time>0) {
            self.game.quiz.time--;
            self.timeLabel.attributedText = [self getTimerLabel];
            if(self.game.quiz.time==0)
            {
                self.levelLabel.backgroundColor = _timeoutColor;
                self.timeLabel.backgroundColor = _timeoutColor;
                self.highLabel.backgroundColor = _timeoutColor;
                self.maxLabel.backgroundColor = _timeoutColor;
                self.homeButton.backgroundColor = _timeoutColor;
            }
        }
    }
}

-(UIImage *)resize:(UIImage *)image
{
    float width = self.choice1Button.bounds.size.width;
    float height = self.choice1Button.bounds.size.height;
    UIGraphicsBeginImageContextWithOptions(self.choice1Button.bounds.size, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, width, height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(void)setImage:(enum QuizType)type
{
    switch(type)
    {
        case Capitals:
            self.imageView.image = [UIImage imageNamed:@"CapitalsButtonIcon.png"];
            break;
        case Flags:
            self.imageView.image = [UIImage imageNamed:@"FlagsButtonIcon.png"];
            break;
        case Currencies:
            self.imageView.image = [UIImage imageNamed:@"CurrenciesButtonIcon.png"];
            break;
        case Continents:
            self.imageView.image = [UIImage imageNamed:@"ContinentsButtonIcon.png"];
            break;
        case Maps:
            self.imageView.image = [UIImage imageNamed:@"MapsButtonIcon.png"];
            break;
        case QuizTypeNone:
            break;
    }
}

#pragma mark Choice Button methods
-(void)updateButtons
{
    self.choice1Button.enabled
    = self.choice2Button.enabled
    = self.choice3Button.enabled
    = self.choice4Button.enabled
    = YES;
    
    if(!_drawImageInChoice)
    {
        [self.choice1Button setTitle:self.game.quiz.choice1 forState:UIControlStateNormal];
        [self.choice2Button setTitle:self.game.quiz.choice2 forState:UIControlStateNormal];
        [self.choice3Button setTitle:self.game.quiz.choice3 forState:UIControlStateNormal];
        [self.choice4Button setTitle:self.game.quiz.choice4 forState:UIControlStateNormal];
    }
    else
    {
        [self updateButton:self.choice1Button
                 imageName:self.game.quiz.choice1
                  drawType:0
                  gameType:self.game.quiz.type];
        [self updateButton:self.choice2Button
                 imageName:self.game.quiz.choice2
                  drawType:0
                  gameType:self.game.quiz.type];
        [self updateButton:self.choice3Button
                 imageName:self.game.quiz.choice3
                  drawType:0
                  gameType:self.game.quiz.type];
        [self updateButton:self.choice4Button
                 imageName:self.game.quiz.choice4
                  drawType:0
                  gameType:self.game.quiz.type];
    }
    
    NSArray *buttonArray = @[self.choice1Button, self.choice2Button, self.choice3Button, self.choice4Button];
    for(int i=0; i < 4; i++)
    {
        BOOL enabled = [self.game.quiz.enabledArray[i] boolValue];
        if(!enabled) {
            UIButton *button = (UIButton *)buttonArray[i];
            button.enabled = NO;
            if(_drawImageInChoice)
            {
                [self updateButton:button
                          drawType:2
                          gameType:self.game.quiz.type];
            }
            else
            {
                button.backgroundColor = noMatchColor;
            }
        }
    }
}

-(void)updateButton:(UIButton *)button drawType:(int)drawType gameType:(enum QuizType)gameType
{
    switch(button.tag)
    {
        case 0:
            [self updateButton:button
                     imageName:self.game.quiz.choice1
                      drawType:drawType
                      gameType:gameType];
            break;
        case 1:
            [self updateButton:button
                     imageName:self.game.quiz.choice2
                      drawType:drawType
                      gameType:gameType];
            break;
        case 2:
            [self updateButton:button
                     imageName:self.game.quiz.choice3
                      drawType:drawType
                      gameType:gameType];
            break;
        case 3:
            [self updateButton:button
                     imageName:self.game.quiz.choice4
                      drawType:drawType
                      gameType:gameType];
            break;
    }
}

-(void)updateButton:(UIButton *)button imageName:(NSString *)imageName drawType:(int)drawType gameType:(enum QuizType)gameType
{
    NSString *path = nil;
    if(gameType==Flags)
    {
        path = [[NSBundle mainBundle] pathForResource:imageName
                                        ofType:@"png"
                                   inDirectory:@"flags"];
    }
    else
    {
        path = [[NSBundle mainBundle] pathForResource:imageName
                                        ofType:@"png"
                                   inDirectory:@"maps"];
    }
    
    UIImage *image = [UIImage imageWithContentsOfFile:path];
    float scale = image.size.width / image.size.height;
    CGSize size = button.frame.size;
    float labelScale = (size.width - 2*kImageMargin) / (size.height - 2*kImageMargin);
    UIGraphicsBeginImageContext( size );
    
    // draw the original flag background
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect rect = CGRectMake(0,0,size.width, size.height);
    if(drawType==0)
    {
        if(gameType==Flags)
            [self.flagImage drawInRect:rect];
        else
            [self.mapImage drawInRect:rect];
    }
    else if(drawType==1)
    {
        CGContextSetFillColorWithColor(context, [matchColor CGColor]);
        CGContextFillRect(context, rect);
    }
    else if(drawType==2)
    {
        CGContextSetFillColorWithColor(context, [noMatchColor CGColor]);
        CGContextFillRect(context, rect);
    }
    
    // scale the flag and draw within a narrow boundary
    if(scale > labelScale)
    {
        float newHeight = (size.width - 2*kImageMargin) / scale;
        float newOrigin = (size.height - newHeight)/2;
        [image drawInRect:CGRectMake(kImageMargin,newOrigin,size.width - 2*kImageMargin,newHeight)];
    }
    else
    {
        float newWidth = (size.height - 2*kImageMargin)* scale;
        float newOrigin = (size.width-newWidth)/2;
        [image drawInRect:CGRectMake(newOrigin,kImageMargin,newWidth,size.height-2*kImageMargin)];
    }
    
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    button.backgroundColor = [UIColor colorWithPatternImage:newImage];
}

-(void)updateLabel:(UILabel *)label withImageName:(NSString *)imageName forQuizType:(enum QuizType)quizType
{
    NSString *path = nil;
    if(quizType==Flags)
    {
        path = [[NSBundle mainBundle] pathForResource:imageName
                                               ofType:@"png"
                                          inDirectory:@"flags"];
    }
    else
    {
        path = [[NSBundle mainBundle] pathForResource:imageName
                                               ofType:@"png"
                                          inDirectory:@"maps"];
    }
    
    UIImage *image = [UIImage imageWithContentsOfFile:path];
    float scale = image.size.width / image.size.height;
    CGSize size = label.frame.size;
    float labelScale = size.width / size.height;
    
    UIGraphicsBeginImageContext( size );
    
    // scale the flag and draw within a narrow boundary
    if(scale > labelScale)
    {
        float newHeight = size.width / scale;
        float newOrigin = (size.height - newHeight)/2;
        [image drawInRect:CGRectMake(0,newOrigin,size.width,newHeight)];
    }
    else
    {
        float newWidth = size.height* scale;
        float newOrigin = (size.width-newWidth)/2;
        [image drawInRect:CGRectMake(newOrigin,0,newWidth,size.height)];
    }
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    label.text = @"";
    label.backgroundColor = [UIColor colorWithPatternImage:newImage];
}

#pragma mark Panel Label methods
-(void)initLabels
{
    translation = [[Translation alloc] init];
    self.timeText = [translation getTranslation:@"Time"];
    self.levelText = [translation getTranslation:@"Level"];
    self.maxText = [translation getTranslation:@"Max"];
    self.scoreText = [translation getTranslation:@"Score"];
    
    static NSString *iPhoneFontKey = @"iPhone Game Label Header Font";
    static NSString *iPadFontKey = @"iPad Game Label Header Font";
    iPhoneHeaderFont = [[[NSBundle mainBundle] objectForInfoDictionaryKey:iPhoneFontKey] floatValue];
    iPadHeaderFont = [[[NSBundle mainBundle] objectForInfoDictionaryKey:iPadFontKey] floatValue];
    
    _panelColor = [UIColor colorWithRed:0
                                  green:33/256.0
                                   blue:81/256.0
                                  alpha:1];
    
    _timeoutColor = [UIColor colorWithRed:187/256.0
                                    green:21/256.0
                                     blue:27/256.0
                                    alpha:1];
}

-(NSAttributedString *)getLevelLabel
{
    NSInteger len = [self.levelText length];
    NSMutableAttributedString *labelText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n \n%d", self.levelText, self.game.level]];
    [labelText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:headerFontSize] range:NSMakeRange(0,len)];
     [labelText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:5.0F] range:NSMakeRange(len+1,1)];
    return labelText;
}

-(NSAttributedString *)getTimerLabel
{
    NSInteger len = [self.timeText length];
    NSMutableAttributedString *labelText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n \n%d", self.timeText, self.game.quiz.time]];
    [labelText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:headerFontSize] range:NSMakeRange(0,len)];
     [labelText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:5.0F] range:NSMakeRange(len+1,1)];
    return labelText;
}

-(NSAttributedString *)getScoreLabel
{
    NSInteger len = [self.scoreText length];
    NSMutableAttributedString *labelText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n \n%d", self.scoreText, self.game.score]];
    [labelText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:headerFontSize] range:NSMakeRange(0,len)];
    [labelText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:5.0F] range:NSMakeRange(len+1,1)];
    return labelText;
}

-(NSAttributedString *)getMaxLabel
{
    NSInteger len = [self.maxText length];
    NSMutableAttributedString *labelText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n \n%d", self.maxText, self.game.maxPoints]];
    [labelText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:headerFontSize] range:NSMakeRange(0,len)];
    [labelText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:5.0F] range:NSMakeRange(len+1,1)];
    return labelText;
}

- (IBAction)showMenu:(id)sender {
    [self pauseGame];
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)tapAnswer:(id)sender
{
    if(startNewLevel)
        return;
    
    UIButton *button = (UIButton *)sender;
    if(button!=nil) {
        if(button.tag==self.game.quiz.answer) {
            self.view.userInteractionEnabled = false;
            self.homeButton.enabled = false;
            self.game.quiz.attempts++;
            if(!_drawImageInChoice)
            {
                button.backgroundColor =  matchColor;
            }
            else
            {
                [self updateButton:button
                          drawType:1
                          gameType:self.game.quiz.type];
            }
            
            [self.audioHelper playMatchSound];
            int points =    [Points calculate:self.game.quiz.type
                                      attempt:self.game.quiz.attempts
                                         time:self.game.quiz.time
                                   difficulty:self.game.quiz.difficulty];
            self.game.score += points;
            self.highLabel.attributedText = [self getScoreLabel];
            startNewLevel = true;
        } else {
            if([self.game.quiz.enabledArray[button.tag] boolValue]) {
                if(!_drawImageInChoice)
                {
                    button.backgroundColor = noMatchColor;
                }
                else
                {
                    [self updateButton:button drawType:2
                              gameType:self.game.quiz.type];
                }
                self.game.quiz.attempts++;
                [self.audioHelper playNoMatchSound];
                self.game.quiz.enabledArray[button.tag] = @NO;
            }
        }
    }
}
@end
