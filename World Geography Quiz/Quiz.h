//
//  Quiz.h
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 20/01/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import <Foundation/Foundation.h>

enum QuizType
{
    Capitals,
    Flags,
    Currencies,
    Continents,
    Maps,
    QuizTypeNone // Used with GameType=Full
};

@interface Quiz : NSObject
@property (strong, nonatomic) NSString *question;
@property (strong, nonatomic) NSString *choice1;
@property (strong, nonatomic) NSString *choice2;
@property (strong, nonatomic) NSString *choice3;
@property (strong, nonatomic) NSString *choice4;
@property (strong, nonatomic) NSString *prompt;
@property int answer;
@property enum QuizType type;
@property int level;
@property int difficulty;
@property int attempts;
@property int time;
@property (assign, nonatomic) BOOL reverse;
@property NSMutableArray *enabledArray;
@end
