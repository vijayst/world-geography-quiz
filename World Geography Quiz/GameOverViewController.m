//
//  GameOverViewController.m
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 16/03/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import "GameOverViewController.h"
#import "Game.h"
#import "Points.h"
#import "Translation.h"
#import <Social/Social.h>

@interface GameOverViewController ()
{
    Translation *translation;
}
@end

@implementation GameOverViewController
{
    NSArray *_buttons;
    NSString *_shareText;
    NSURL *_shareUrl;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initLabels];
    [self initButtons];
     self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]];
}

-(void)initLabels
{
    translation = [[Translation alloc] init];
    NSString *titleText = [translation getTranslation:@"Game Over"];
    self.titleLabel.text = titleText;
    NSString *mainText = [translation getTranslation:@"Main Menu"];
    [self.mainButton setTitle:mainText forState:UIControlStateNormal];
}

-(void)initButtons
{
    _buttons = @[self.facebookButton, self.twitterButton, self.mainButton];
    BOOL iPhone = [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone;
    for(UIButton *button in _buttons)
    {
        button.layer.cornerRadius = iPhone ? 6.0F : 10.0F;
        button.layer.borderWidth = 1.0F;
        button.layer.borderColor = [[UIColor lightTextColor] CGColor];
    }
    NSString *facebookText = [translation getTranslation:@"Share with Facebook"];
    [self.facebookButton setTitle:facebookText forState:UIControlStateNormal];
    NSString *twitterText = [translation getTranslation:@"Share with Twitter"];
    [self.twitterButton setTitle:twitterText forState:UIControlStateNormal];
}

-(void)viewDidAppear:(BOOL)animated
{
    self.facebookButton.hidden = ![SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook];
    self.twitterButton.hidden = ![SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter];
    
    self.game.score += [Points getBonusPoints:self.game.gameType quizType:self.game.quiz.type];
    int maxPoints = [Points getMaximumPoints:self.game.gameType quizType:self.game.quiz.type];
    
    NSString *gameOverText = [translation getTranslation:@"Congrats! You scored %1$d out of %2$d points."];
    self.gameOverLabel.text = [NSString stringWithFormat:gameOverText, self.game.score, maxPoints];
    NSString *shareTextFormat = [translation getTranslation:@"I scored %1$d out of %2$d points in %3$@."];
    NSString *productName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    _shareText = [NSString stringWithFormat:shareTextFormat, self.game.score, maxPoints, productName];
    NSString *urlText = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"App Url"];
    _shareUrl = [NSURL URLWithString:urlText];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)showMenu:(id)sender {
    if(self.presentingViewController!=nil)
        [self.presentingViewController dismissViewControllerAnimated:true completion:nil];
}
- (IBAction)shareFacebook:(id)sender {
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [controller setInitialText:_shareText];
        [controller addURL:_shareUrl];
        [self presentViewController:controller animated:YES completion:Nil];
    }
}

- (IBAction)shareTwitter:(id)sender {
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [controller setInitialText:_shareText];
        [controller addURL:_shareUrl];
        [self presentViewController:controller animated:YES completion:Nil];
    }
}
@end
