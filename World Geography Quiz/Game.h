//
//  Game.h
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 20/01/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Quiz.h"

enum GameType
{
    GameTypeSingle,
    GameTypeMulti
};

@interface Game : NSObject
{
}

@property (readonly, nonatomic) enum GameType gameType;
@property int score;
@property int level;
@property BOOL paused;
@property BOOL over;
@property (readonly) int maxPoints;
@property (strong, nonatomic) Quiz* quiz;
@property (strong, nonatomic) NSArray *countries;

-(BOOL)getQuiz;
+(Game *)createGame:(enum GameType)gameType andType:(enum QuizType)quizType;
@end
