//
//  ViewController.h
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 20/01/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Game.h"
#import "Quiz.h"
#import "AnimationHelper.h"
#import "TTSHelper.h"

@interface GameViewController : UIViewController<AnimationCompletedDelegate, SpeechCompletedDelegate, UIAlertViewDelegate>
{
    bool animComplete;
    bool startNewLevel;
    int startNewLevelTime;
    NSTimer *timer;
    bool iPhone;
    float headerFontSize;
    
    UIColor *matchColor;
    UIColor *noMatchColor;
}

@property (strong, nonatomic) IBOutlet UILabel *questionLabel;
@property (strong, nonatomic) IBOutlet UILabel *levelLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UILabel *highLabel;
@property (strong, nonatomic) IBOutlet UILabel *maxLabel;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIButton *homeButton;
@property (strong, nonatomic) IBOutlet UILabel *questionInnerLabel;
@property (strong, nonatomic) IBOutlet UIButton *choice1Button;
@property (strong, nonatomic) IBOutlet UIButton *choice2Button;
@property (strong, nonatomic) IBOutlet UIButton *choice3Button;
@property (strong, nonatomic) IBOutlet UIButton *choice4Button;
- (IBAction)showMenu:(id)sender;
- (IBAction)tapAnswer:(id)sender;

@property (strong, nonatomic) Game* game;
@end
