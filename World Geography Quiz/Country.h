//
//  Country.h
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 20/01/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Country : NSObject
@property int difficulty;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *capital;
@property (copy, nonatomic) NSString *flag;
@property (copy, nonatomic) NSString *currency;
@property (copy, nonatomic) NSString *continent;
@property (copy, nonatomic) NSString *map;

+(NSArray *)getCountries;

@end
