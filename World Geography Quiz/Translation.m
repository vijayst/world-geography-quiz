//
//  Translation.m
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 09/11/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import "Translation.h"
#import "DbHelper.h"

@interface Translation()
{
    BOOL enabled;
    DbHelper *dbHelper;
}
@end

@implementation Translation

-(id)init
{
    self = [super init];
    if(self!=nil)
    {
        dbHelper = [[DbHelper alloc] init];
        [dbHelper open];
        enabled = [dbHelper doesTableExist:@"labels"];
    }
    return self;
}

-(NSString *)getTranslation:(NSString *)englishString
{
    NSString *translated = englishString;
    if(enabled)
    {
        NSString *sql = [NSString stringWithFormat:@"SELECT translated FROM labels WHERE english = '%@'", englishString];
        sqlite3_stmt *statement;
        [dbHelper executeQuery:sql statement:&statement];
        if(sqlite3_step(statement) == SQLITE_ROW)
        {
            translated = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
        }
    }
    return translated;
}

- (void)dealloc {
    [dbHelper close];
}

@end
