//
//  Points.m
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 20/01/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import "Points.h"

@interface Points()
+(int)getCapitalPoints:(int)difficulty attempt:(int)attempt;
+(int)getFlagPoints:(int)difficulty attempt:(int)attempt;
+(int)getCurrencyPoints:(int)difficulty attempt:(int)attempt;
+(int)getContinentPoints:(int)difficulty attempt:(int)attempt;
@end

@implementation Points

+(int)calculate:(enum QuizType)type attempt:(int)attempt time:(int)time difficulty:(int)difficulty
{
    int points = 0;
    switch(type)
    {
        case Capitals:
            points = [self getCapitalPoints:difficulty attempt:attempt];
            break;
        case Flags:
            points = [self getFlagPoints:difficulty attempt:attempt];
            break;
        case Currencies:
            points = [self getCurrencyPoints:difficulty attempt:attempt];
            break;
        case Continents:
            points = [self getContinentPoints:difficulty attempt:attempt];
            break;
        case Maps:
            points = [self getMapPoints:difficulty attempt:attempt];
            break;
        case QuizTypeNone:
            break;
    }
    
    if(time > 12 && attempt==1)
        points += 8;
    else if(time>10 && attempt<=2)
        points += 5;
    else if(time>5 && attempt<=2)
        points += 2;
    
    return points;
}

+(int)getMaximumPoints:(enum GameType)gameType quizType:(enum QuizType)quizType
{
    if(gameType==GameTypeMulti)
        return (kMaxPoints + kBonusPoints) * 10;
    else
    {
        switch(quizType)
        {
            case Capitals:
            case Continents:
                return kMaxPoints + kBonusPoints;
            case Currencies:
                return 2*(kMaxPoints + kBonusPoints);
            case Flags:
            case Maps:
                return  3*(kMaxPoints + kBonusPoints);
            case QuizTypeNone:
                return 0;
        }
    }
    return 0;
}

+(int)getBonusPoints:(enum GameType)gameType quizType:(enum QuizType)quizType
{
    if(gameType==GameTypeMulti)
        return kBonusPoints*10;
    else
    {
        switch(quizType)
        {
            case Capitals:
            case Continents:
                return kBonusPoints;
            case Currencies:
                return 2*(kBonusPoints);
            case Flags:
            case Maps:
                return  3*(kBonusPoints);
            case QuizTypeNone:
                return 0;
        }
    }
}

static const int capitalArray[3][4] = {{ 6, 3, 1, 0 },{ 12, 6, 3, 1 },{ 15, 8, 4, 2 }};
static const int flagArray[3][4] = {{ 18, 9, 3, 0 },{ 36, 18, 9, 3 },{ 45, 24, 12, 6 }};
static const int currencyArray[3][4] = {{ 12, 6, 2, 0 },{ 24, 12, 6, 2 },{ 30, 16, 8, 4 }};
static const int continentArray[3][4] = {{ 6, 3, 1, 0 },{ 12, 6, 3, 1 },{ 15, 8, 4, 2 }};
static const int mapArray[3][4] = {{ 18, 9, 3, 0 },{ 36, 18, 9, 3 },{ 45, 24, 12, 6 }};

static const int kBonusPoints = 45;
static const int kMaxPoints = 4955;

+(int)getCapitalPoints:(int)difficulty attempt:(int)attempt
{
    return capitalArray[difficulty-1][attempt-1];
}

+(int)getFlagPoints:(int)difficulty attempt:(int)attempt
{
    return flagArray[difficulty-1][attempt-1];
}

+(int)getCurrencyPoints:(int)difficulty attempt:(int)attempt
{
    return currencyArray[difficulty-1][attempt-1];
}

+(int)getContinentPoints:(int)difficulty attempt:(int)attempt
{
    return continentArray[difficulty-1][attempt-1];
}

+(int)getMapPoints:(int)difficulty attempt:(int)attempt
{
    return mapArray[difficulty-1][attempt-1];
}

@end
