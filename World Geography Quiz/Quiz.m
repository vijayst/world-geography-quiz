//
//  Quiz.m
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 20/01/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import "Quiz.h"
#import "Game.h"

@implementation Quiz
@synthesize question;
@synthesize choice1;
@synthesize choice2;
@synthesize choice3;
@synthesize choice4;
@synthesize answer;
@synthesize type;
@synthesize prompt;
@synthesize level;
@synthesize difficulty;

-(id)init
{
    self = [super init];
    if(self!=nil) {
        self.attempts = 0;
        self.time = 15;
        self.enabledArray = [@[@YES, @YES, @YES, @YES] mutableCopy];
    }
    return self;
}

@end
