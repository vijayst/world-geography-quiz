//
//  HomeViewController.m
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 16/03/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import "HomeViewController.h"
#import "GameViewController.h"
#import "GameOverViewController.h"
#import "Translation.h"

#import "AudioHelper.h"
#import "Game.h"

@interface HomeViewController ()
{
    Translation *translation;
    GameViewController *_gameController;
    
    Game *_fullGame;
    Game *_capitalGame;
    Game *_flagGame;
    Game *_currencyGame;
    Game *_continentGame;
    Game *_mapGame;
    Game *_currentGame;
}
@property (strong, nonatomic) AudioHelper *audioHelper;
@property (strong, nonatomic) UIDynamicAnimator *animator;
-(void)animate;
-(void)initLabels;
-(void)presentGameController:(Game *)game;
@end

@implementation HomeViewController
{
    NSArray *_buttons;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initLabels];
    [self initButtons];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.png"]];
     CGRect rect = [[UIScreen mainScreen] bounds];
    
    if(rect.size.height==480)
    {
        float topAdjust = -1.5F;
        float heightAdjust = -10.0F;
        float spaceAdjust = -5.0F;
       
        int index = 0;
        for(UIButton *button in _buttons)
        {
            CGRect buttonFrame = button.frame;
            CGFloat yAdjust = topAdjust + index * (heightAdjust + spaceAdjust);
            CGRect newFrame = CGRectMake(buttonFrame.origin.x,
                                         buttonFrame.origin.y + yAdjust,
                                         buttonFrame.size.width,
                                         buttonFrame.size.height + heightAdjust);
            button.frame = newFrame;
            index++;
        }
    }
    
    self.audioHelper = [[AudioHelper alloc] init];
    [self.audioHelper playAppLoadSound];
    [self animate];
}

-(void)animate
{
    CGPoint topRight = CGPointMake(self.fullGameButton.frame.origin.x + self.fullGameButton.frame.size.width,
                                   self.fullGameButton.frame.origin.y);
    CGPoint bottomRight = CGPointMake(self.mapGameButton.frame.origin.x + self.mapGameButton.frame.size.width,
                                      self.mapGameButton.frame.origin.y + self.mapGameButton.frame.size.height);
    
    int index = 1;
    for(UIButton *button in _buttons)
    {
        CGRect buttonFrame = button.frame;
        buttonFrame.origin.x = - buttonFrame.size.width - 200.0 * index;
        button.frame = buttonFrame;
        index++;
    }
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    // Add gravity behavior - horizontal direction
    UIGravityBehavior *gravityBehavior = [[UIGravityBehavior alloc] initWithItems:_buttons];
    gravityBehavior.gravityDirection = CGVectorMake(1.0, 0.0);
    [self.animator addBehavior:gravityBehavior];
    
    // Add collision behavior
    UICollisionBehavior *collisionBehavior = [[UICollisionBehavior alloc] initWithItems:_buttons];
    collisionBehavior.collisionMode = UICollisionBehaviorModeBoundaries;

    [collisionBehavior addBoundaryWithIdentifier:@"stop"
                                       fromPoint:topRight
                                         toPoint:bottomRight];
    [self.animator addBehavior:collisionBehavior];
    
    // Add elasticity
    UIDynamicItemBehavior *dynamicBehavior = [[UIDynamicItemBehavior alloc] initWithItems:_buttons];
    dynamicBehavior.elasticity = 0.25;
    [self.animator addBehavior:dynamicBehavior];
}

-(void)initLabels
{
    translation = [[Translation alloc] init];
    NSString *fullText = [translation getTranslation:@"Full Game"];
    NSString *capitalsText = [translation getTranslation:@"Capitals"];
    NSString *continentsText = [translation getTranslation:@"Continents"];
    NSString *flagsText = [translation getTranslation:@"Flags"];
    NSString *currenciesText = [translation getTranslation:@"Currencies"];
    NSString *mapsText = [translation getTranslation:@"Maps"];
    
    [self.fullGameButton setTitle:fullText forState:UIControlStateNormal];
    [self.capitalGameButton setTitle:capitalsText forState:UIControlStateNormal];
    [self.continentGameButton setTitle:continentsText forState:UIControlStateNormal];
    [self.currencyGameButton setTitle:currenciesText forState:UIControlStateNormal];
    [self.flagGameButton setTitle:flagsText forState:UIControlStateNormal];
    [self.mapGameButton setTitle:mapsText forState:UIControlStateNormal];
}

-(void)initButtons
{
    _buttons = @[self.fullGameButton, self.capitalGameButton, self.flagGameButton,
                self.currencyGameButton, self.continentGameButton, self.mapGameButton];
    for(UIButton *button in _buttons)
    {
        button.titleLabel.textAlignment = NSTextAlignmentCenter;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)fullGameClick:(id)sender {
    if(_fullGame==nil || _fullGame.over)
        _fullGame = [Game createGame:GameTypeMulti andType:QuizTypeNone];
    [self presentGameController:_fullGame];
}

- (IBAction)capitalGameClick:(id)sender {
    if(_capitalGame==nil || _capitalGame.over)
        _capitalGame = [Game createGame:GameTypeSingle andType:Capitals];
    [self presentGameController:_capitalGame];
}

- (IBAction)flagGameClick:(id)sender {
    if(_flagGame==nil || _flagGame.over)
        _flagGame = [Game createGame:GameTypeSingle andType:Flags];
    [self presentGameController:_flagGame];
}

- (IBAction)currencyGameClick:(id)sender {
    if(_currencyGame==nil || _currencyGame.over)
        _currencyGame = [Game createGame:GameTypeSingle andType:Currencies];
    [self presentGameController:_currencyGame];
}

- (IBAction)continentGameClick:(id)sender {
    if(_continentGame==nil || _continentGame.over)
        _continentGame = [Game createGame:GameTypeSingle andType:Continents];
    [self presentGameController:_continentGame];
}

- (IBAction)mapGameClick:(id)sender {
    if(_mapGame==nil || _mapGame.over)
        _mapGame = [Game createGame:GameTypeSingle andType:Maps];
    [self presentGameController:_mapGame];
}


-(void)presentGameController:(Game *)game
{
    _currentGame = game;
    if(_gameController==nil) {
        _gameController = [self.storyboard instantiateViewControllerWithIdentifier:@"gameVcId"];
    }
    _gameController.game = game;
    [self presentViewController:_gameController
                       animated:NO
                     completion:nil];
}

-(void)showGameOver
{
    GameOverViewController *overController = [self.storyboard instantiateViewControllerWithIdentifier:@"overVcId"];
    overController.game = _currentGame;
    [self presentViewController:overController
                       animated:YES
                     completion:nil];
}

@end
