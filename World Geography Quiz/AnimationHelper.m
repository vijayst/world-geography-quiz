//
//  AnimationHelper.m
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 21/01/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import "AnimationHelper.h"

@implementation AnimationHelper

@synthesize delegate;

-(void) zoom:(UIView *)view
{
    view.transform = CGAffineTransformMakeScale(0.2, 0.2);
	[UIView animateWithDuration:1.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         view.transform = CGAffineTransformIdentity;
                     }
                     completion:^(BOOL finished){
                             [delegate animationCompleted];
                     }];
}

-(void) fade:(UIView *)view
{
    [view setAlpha:0.1];
	[UIView animateWithDuration:2.0 delay:0.0 options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         [view setAlpha:1];
                     }
                     completion:^(BOOL finished){
                             [delegate animationCompleted];
                     }];
}

@end
