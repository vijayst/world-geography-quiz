//
//  DbHelper.m
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 05/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import "DbHelper.h"


@interface DbHelper()
{
    sqlite3 *database;
    BOOL isOpen;
}
@property (strong, nonatomic) NSString *dbPath;
@end

@implementation DbHelper

-(id)init
{
    if ((self = [super init])) {
        self.dbName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"Database name"];;
        self.dbPath = [NSString stringWithFormat:@"%@.sqlite", self.dbName];
        isOpen = NO;
    }
    return self;
}

-(id)initWithDb:(NSString *)dbName
{
    if ((self = [super init])) {
        self.dbName = dbName;
        self.dbPath = [NSString stringWithFormat:@"%@.sqlite", dbName];
        isOpen = NO;
    }
    return self;
}

-(void)copyDbToAppSupport:(BOOL)overwrite
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *supportDir = [NSSearchPathForDirectoriesInDomains
                            (NSApplicationSupportDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    if(![fileManager fileExistsAtPath:supportDir])
    {
        [fileManager    createDirectoryAtPath:supportDir
                  withIntermediateDirectories:NO
                                   attributes:nil
                                        error:nil];
    }
    
    NSString *supportPath = [supportDir stringByAppendingPathComponent:self.dbPath];
    BOOL isDir;
    if((![fileManager fileExistsAtPath:supportPath isDirectory:&isDir]) || overwrite){
        NSString *bundlePath = [[NSBundle mainBundle] pathForResource:self.dbName
                                                               ofType:@"sqlite"];
        [fileManager copyItemAtPath:bundlePath
                             toPath:supportPath error:nil];
    }
}

-(void)open
{
    // Get it from the main bundle instead.
    // NSString *supportDir = [NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    // NSString *dbFullPath = [supportDir stringByAppendingPathComponent:self.dbPath];
    NSString *dbFullPath = [[NSBundle mainBundle] pathForResource:self.dbName
                                                           ofType:@"sqlite"];
    
    if (sqlite3_open([dbFullPath UTF8String], &database) != SQLITE_OK) {
        NSLog(@"Failed to open database!");
        char *errorMessage = (char *)sqlite3_errmsg(database);
        NSLog(@"%@", [NSString stringWithUTF8String:errorMessage]);
    }
    else {
        isOpen = YES;
    }
}

-(void)close
{
    if(isOpen)
    {
        sqlite3_close(database);
        isOpen = NO;
    }
}

- (void)dealloc {
    [self close];
}

-(bool)executeCommand:(NSString *)sql
{
    sqlite3_stmt *statement;
    const char *sql_stmt = [sql UTF8String];
    if(sqlite3_prepare_v2(database, sql_stmt,
                          -1, &statement, NULL) == SQLITE_OK) {
        bool done = sqlite3_step(statement) == SQLITE_DONE;
        sqlite3_finalize(statement);
        return done;
    }
    return false;
}

-(bool)executeQuery:(NSString *)sql statement:(sqlite3_stmt **)statement
{
    const char *sql_stmt = [sql UTF8String];
    return sqlite3_prepare_v2(database, sql_stmt,
                              -1, statement, NULL) == SQLITE_OK;
}

-(float)executeReal:(NSString *)sql
{
    float result = 0;
    sqlite3_stmt *statement;
    const char *sql_stmt = [sql UTF8String];
    if(sqlite3_prepare_v2(database, sql_stmt,
                          -1, &statement, NULL) == SQLITE_OK)
    {
        if(sqlite3_step(statement) == SQLITE_ROW)
        {
            result = (float)sqlite3_column_double(statement, 0);
        }
        sqlite3_finalize(statement);
    }
    return result;
}

-(BOOL)doesTableExist:(NSString *)tableName
{
    BOOL result = NO;
    NSString *sql = [NSString stringWithFormat:@"SELECT name FROM sqlite_master WHERE type='table' AND name='%@';", tableName];
    sqlite3_stmt *statement;
    if([self executeQuery:sql statement:&statement])
    {
        if(sqlite3_step(statement) == SQLITE_ROW)
        {
            NSString *resultName = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, 0)];
            result = [resultName isEqualToString:tableName];
        }
    }
    return result;
}

@end

