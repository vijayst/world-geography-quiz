//
//  Game.m
//  World Geography Quiz
//
//  Created by Vijay Thirugnanam on 20/01/14.
//  Copyright (c) 2014 Fun Studyo. All rights reserved.
//

#import "Game.h"
#import "Country.h"
#import "Points.h"
#import "Translation.h"

@interface Game()
{
    enum GameType _gameType;
    enum QuizType _quizType;
    int _answerPosition; // 0 - 3
    int _question; // array position
    int _count;
}
@property (strong, nonatomic) NSMutableArray *gameArray;
@property (strong, nonatomic) NSMutableArray *typeArray;
@property (nonatomic, strong) Translation *translation;

-(void)setGameType:(enum GameType)gameType andQuizType:(enum QuizType)quizType;
-(NSMutableArray *)getCapitals:(NSString *)answer;
-(NSMutableArray *)getFlags:(NSString *)answer;
-(NSMutableArray *)getCurrencies:(NSString *)answer;
-(NSMutableArray *)getContinents:(NSString *)answer;
@end

@implementation Game

@synthesize typeArray;
@synthesize gameArray;
@synthesize countries;
@synthesize score;
@synthesize  level;


+(Game *)createGame:(enum GameType)gameType andType:(enum QuizType)quizType
{
    Game* newGame = [[Game alloc] init];
    if(newGame) {
        [newGame setGameType:gameType andQuizType:quizType];
    }
    return newGame;
}

-(id)init
{
    self = [super init];
    if(self!=nil)
    {
        self.translation = [[Translation alloc] init];
        self.level = 0;
        self.score = 0;
        self.countries = [Country getCountries];
        _count = (int)[self.countries count];
        NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:_count];
        for (int i=0; i<_count; i++) {
            array[i] = [NSNumber numberWithInt:i];
        }
        self.gameArray = [NSMutableArray arrayWithArray:@[array,
                                                          [array mutableCopy],
                                                          [array mutableCopy],
                                                          [array mutableCopy],
                                                          [array mutableCopy]]];
    }
    return self;
}


-(enum GameType)gameType
{
    return _gameType;
}

-(int)maxPoints
{
    return [Points getMaximumPoints:_gameType quizType:_quizType];
}

-(void)setGameType:(enum GameType)gameType andQuizType:(enum QuizType)quizType
{
    _gameType = gameType;
    _quizType = quizType;
    
    switch(gameType)
    {
        case GameTypeSingle:
            switch(quizType)
            {
                case Capitals:
                    typeArray = [NSMutableArray arrayWithArray:@[[NSNumber numberWithInt:Capitals]]];
                    break;
                case Flags:
                    typeArray = [NSMutableArray arrayWithArray:@[[NSNumber numberWithInt:Flags]]];
                    break;
                case Currencies:
                    typeArray = [NSMutableArray arrayWithArray:@[[NSNumber numberWithInt:Currencies]]];
                    break;
                case Continents:
                    typeArray = [NSMutableArray arrayWithArray:@[[NSNumber numberWithInt:Continents]]];
                    break;
                case Maps:
                    typeArray = [NSMutableArray arrayWithArray:@[[NSNumber numberWithInt:Maps]]];
                case QuizTypeNone:
                    break;
            }
            break;
        case GameTypeMulti:
            typeArray = [NSMutableArray arrayWithArray:@[[NSNumber numberWithInt:Capitals],
                                                         [NSNumber numberWithInt:Flags],
                                                         [NSNumber numberWithInt:Currencies],
                                                         [NSNumber numberWithInt:Continents],
                                                         [NSNumber numberWithInt:Maps]]];
            break;
    }
}


-(BOOL)getQuiz
{
    // no more quizzes
    if(self.over)
        return NO;
    
    level++;
    int questionType = _quizType;
    if(_gameType == GameTypeMulti) {
        int questionTypeIndex = arc4random() % [typeArray count];
        questionType = [self.typeArray[questionTypeIndex] intValue];
    }
    
    NSMutableArray *quizItems = [gameArray objectAtIndex:questionType];
    int questionNumberIndex = arc4random() % [quizItems count];
    NSNumber *questionNumber = [quizItems objectAtIndex:questionNumberIndex];
    [quizItems removeObjectAtIndex:questionNumberIndex];
    // if last element in the sequence, that sequence is complete.
    if([quizItems count]==0) {
        if(_gameType==GameTypeMulti) {
            [gameArray removeObjectAtIndex:questionType];
            [typeArray removeObjectAtIndex:questionType];
            if([typeArray count]==0) {
                self.over = YES;
            }
        }
        else {
            self.over = YES;
        }
    }
    
    _question = [questionNumber intValue];
    Country *country = [countries objectAtIndex:_question];
    BOOL reverse = _question % 3 == 0;
    
    self.quiz = [[Quiz alloc] init];
    NSMutableArray *answerArray;
    NSString *prompt;
    
    // question type is different from game type.
    // game type can be full
    // question type is 0 - 3.
    if(reverse)
    {
        switch(questionType)
        {
            case Capitals:
                answerArray = [self getCountries:country.capital
                                      withAnswer:country.name
                                     forQuizType:questionType];
                prompt = [self.translation getTranslation:@"What is the country with the capital of %@?"];
                self.quiz.question = country.capital;
                prompt = [NSString stringWithFormat:prompt, country.capital];
                break;
            case Flags:
                answerArray = [self getCountries:country.flag
                                      withAnswer:country.name
                                     forQuizType:questionType];
                prompt = [self.translation getTranslation:@"What is the country whose flag is shown?"];
                self.quiz.question = country.flag;
                break;
            case Currencies:
                answerArray = [self getCountries:country.currency
                                      withAnswer:country.name
                                     forQuizType:questionType];
                prompt = [self.translation getTranslation:@"What is the country with the currency of %@?"];
                self.quiz.question = country.currency;
                prompt = [NSString stringWithFormat:prompt, country.currency];
                break;
            case Continents:
                answerArray = [self getCountries:country.continent
                                      withAnswer:country.name
                                     forQuizType:questionType];
                prompt = [self.translation getTranslation:@"What is the country which belongs to the %@ continent?"];
                self.quiz.question = country.continent;
                prompt = [NSString stringWithFormat:prompt, country.continent];
                break;
            case Maps:
                answerArray = [self getCountries:country.map
                                      withAnswer:country.name
                                     forQuizType:questionType];
                prompt = [self.translation getTranslation:@"What is the country whose map is shown?"];
                self.quiz.question = country.map;
                break;
            default:
                break;
        }
    }
    else {
        switch(questionType)
        {
            case Capitals:
                answerArray = [self getCapitals:country.capital];
                prompt = [self.translation getTranslation:@"What is the capital of %@?"];
                break;
            case Flags:
                answerArray = [self getFlags:country.flag];
                prompt = [self.translation getTranslation:@"Identify the flag of %@?"];
                break;
            case Currencies:
                answerArray = [self getCurrencies:country.currency];
                prompt = [self.translation getTranslation:@"What is the currency of %@?"];
                break;
            case Continents:
                answerArray = [self getContinents:country.continent];
                prompt = [self.translation getTranslation:@"Which continent does %@ belong to?"];
                break;
            case Maps:
                answerArray = [self getMaps:country.map];
                prompt = [self.translation getTranslation:@"Identify the map of %@?"];
                break;
            default:
                break;
        }
        
        self.quiz.question = country.name;
        prompt = [NSString stringWithFormat:prompt, country.name];
    }
    
    self.quiz.choice1 = [answerArray objectAtIndex:0];
    self.quiz.choice2 = [answerArray objectAtIndex:1];
    self.quiz.choice3 = [answerArray objectAtIndex:2];
    self.quiz.choice4 = [answerArray objectAtIndex:3];
    self.quiz.answer = _answerPosition;
    self.quiz.type = questionType;
    self.quiz.prompt = prompt;
    self.quiz.level = level;
    self.quiz.difficulty = country.difficulty;
    self.quiz.reverse = reverse;
    return YES;
}

-(NSMutableArray *)getCountries:(NSString *)question withAnswer:(NSString *)answer forQuizType:(enum QuizType)quizType
{
    NSMutableSet *answerSet = [[NSMutableSet alloc] init];
    while([answerSet count] < 3)
    {
        Country *country = [countries objectAtIndex:arc4random() % _count];
        if(![country.name isEqualToString:answer])
        {
            BOOL skip = NO;
            switch(quizType)
            {
                case Capitals:
                    skip = [country.capital isEqualToString:question];
                    break;
                case Currencies:
                    skip = [country.currency isEqualToString:question];
                    break;
                case Continents:
                    skip = [country.continent isEqualToString:question];
                    break;
                case Flags:
                case Maps:
                case QuizTypeNone:
                    break;
            }
            if(!skip)
            {
                [answerSet addObject:country.name];
            }
        }
    }
    NSMutableArray *answerArray = [[answerSet allObjects] mutableCopy];
    _answerPosition = arc4random() % 4;
    [answerArray insertObject:answer atIndex:_answerPosition];
    return answerArray;
}

-(NSMutableArray *)getCapitals:(NSString *)answer
{
    return [self getItems:^(Country *c)
            {
                return c.capital;
            } withAnswer:answer];
}

-(NSMutableArray *)getFlags:(NSString *)answer
{
    return [self getItems:^(Country *c)
            {
                return c.flag;
            } withAnswer:answer];
}

-(NSMutableArray *)getCurrencies:(NSString *)answer
{
    return [self getItems:^(Country *c)
            {
                return c.currency;
            } withAnswer:answer];
}

-(NSMutableArray *)getContinents:(NSString *)answer
{
    return [self getItems:^(Country *c)
     {
         return c.continent;
     } withAnswer:answer];
}

-(NSMutableArray *)getMaps:(NSString *)answer
{
    return [self getItems:^(Country *c)
            {
                return c.map;
            } withAnswer:answer];
}

-(NSMutableArray *)getItems:(NSString *(^)(Country *c))lambda withAnswer:(NSString *)answer
{
    NSMutableSet *answerSet = [[NSMutableSet alloc] init];
    while([answerSet count] < 3) {
        Country *country = [countries objectAtIndex:arc4random() % _count];
        NSString *choice = lambda(country);
        if(![choice isEqualToString:answer])
            [answerSet addObject:choice];
    }
    NSMutableArray *answerArray = [[answerSet allObjects] mutableCopy];
    _answerPosition = arc4random() % 4;
    [answerArray insertObject:answer atIndex:_answerPosition];
    return answerArray;
}


@end
